#!/usr/bin/env python3
import argparse
import logging
from pathlib import Path

from kdbx import KDBXExporter
from log import init_logging
from pif import Parser1PIF

logger = logging.getLogger(__name__)


def main():
    options = parse_args()
    init_logging(options.verbosity - options.quietness)
    convert1piftokdbx(options)


def convert1piftokdbx(options):
    logger.debug('log')

    parser = Parser1PIF(Path(options.folder))
    parser.parse()

    exporter = KDBXExporter(parser,
                            options.target,
                            keyfile=options.keyfile,
                            password=options.password)
    exporter.export()


def parse_args():
    parser = argparse.ArgumentParser(
        prog='onepass2kdbx',
        description='Convert 1pif to kdbx.')
    parser.add_argument('folder', type=str, help='source folder (.1pif)')
    parser.add_argument('target', type=str, help='target file (.kdbx)')
    parser.add_argument('-p', '--password', type=str,
                        help='specify kdbx password')
    parser.add_argument('-k', '--keyfile', type=str,
                        help='specify kdbx keyfile')
    parser.add_argument('-v', '--verbose', help='Increase verbosity',
                        dest='verbosity', action='count', default=0)
    parser.add_argument('-q', '--quiet', help='Decrease verbosity',
                        dest='quietness', action='count', default=0)
    return parser.parse_args()


if __name__ == '__main__':
    main()
