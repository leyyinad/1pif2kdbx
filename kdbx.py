import logging
from pathlib import Path
from typing import Dict, Optional

from pykeepass import PyKeePass, create_database
from pykeepass.entry import Entry
from pykeepass.group import Group

from pif.parse1pif import PIF_TYPE_FOLDER, Parser1PIF
from pif.pifentry import PifEntry

logger = logging.getLogger(__name__)


class KDBXExporter:
    def __init__(
            self,
            parser: Parser1PIF,
            target: str,
            keyfile: str,
            password: str = None):
        self.parser = parser
        self.target = target
        self.keyfile = keyfile
        self.password = password
        self.kp: Optional[PyKeePass] = None
        self.groups: Dict[str, Group] = dict()

    def export(self):
        logging.debug('creating "%s"', self.target)
        self.kp = create_database(
            self.target,
            keyfile=self.keyfile,
            password=self.password)
        self.export_groups()
        self.export_entries()
        self.kp.save()

    def export_groups(self):
        for uuid in self.parser.pif_folders.keys():
            self.get_group_by_uuid(uuid)

    def export_entries(self) -> None:
        for entry in self.parser.pif_entries:
            if entry.typeName != PIF_TYPE_FOLDER:
                self.export_entry(entry)

    def export_entry(self, entry: PifEntry) -> None:
        if self.kp is None:
            return

        if entry.folderUuid is None:
            group = self.kp.root_group
        elif entry.folderUuid in self.groups:
            group = self.groups[entry.folderUuid]
        else:
            logger.warn(
                'referenced folder "%s" is missing, using root',
                entry.folderUuid)
            group = self.kp.root_group

        title = entry.title
        username = entry.findUsername()
        password = entry.findPassword()
        url = entry.findUrl()
        notes = entry.findNotes()
        expiry_time = entry.findExpiryTime()
        tags = entry.findTags()

        exported_entry = self.kp.add_entry(
            group,
            title,
            username,
            password,
            url,
            notes,
            expiry_time,
            tags,
            force_creation=True)

        for attachment in entry.attachments:
            binary_id = self.kp.add_binary(
                attachment.data, protected=attachment.protected)
            exported_entry.add_attachment(binary_id, attachment.filename)

    def get_group_by_uuid(self, uuid):
        if uuid not in self.groups:
            folder = self.parser.pif_folders[uuid]

            parent: Optional[Group] = None
            if folder.folderUuid and folder.folderUuid != uuid:
                parent = self.get_group_by_uuid(folder.folderUuid)
            else:
                if isinstance(self.kp.root_group, Group):
                    parent = self.kp.root_group

            notes = folder.findNotes()
            group = self.kp.add_group(parent, folder.title, notes=notes)

            self.groups[uuid] = group

        return self.groups[uuid]
