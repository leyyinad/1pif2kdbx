import logging


def init_logging(verbosity: int = 0):
    level = find_log_level(verbosity)
    level_name = logging.getLevelName(level)
    logging.basicConfig(
        level=level,
        format='%(asctime)s %(name)s %(levelname)s %(message)s')
    logging.debug('log level is %s', level_name)


def find_log_level(verbosity: int):
    if verbosity <= -2:
        return logging.CRITICAL
    elif verbosity == -1:
        return logging.ERROR
    elif verbosity == 0:
        return logging.WARNING
    elif verbosity == 1:
        return logging.INFO
    elif verbosity >= 2:
        return logging.DEBUG

    return logging.NOTSET
