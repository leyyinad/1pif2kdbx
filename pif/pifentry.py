import base64
from base64 import b64decode
from pathlib import Path
from typing import Any, Dict, List, Optional

from pif.attachment import PifAttachment


class PifEntry:
    def __init__(self, data: Dict[str, Any],
                 attachment_dir: Optional[Path] = None) -> None:
        self.attachments: List[PifAttachment] = []
        self.attachment_dir = attachment_dir
        self.data = data

    def __getattr__(self, name: str):
        return self.data.get(name, None)

    def __repr__(self) -> str:
        lines = []
        for k, v in self.data.items():
            lines.append(f"{k:16}: {v}")
        return '\n'.join(lines)

    def collectAttachments(self):
        if not self.attachment_dir or not self.attachment_dir.is_dir():
            return

        attachments = []
        for f in self.attachment_dir.iterdir():
            if f.is_file():
                attachments.append(PifAttachment(f))

        self.attachments = attachments

    def findUsername(self):
        if self.secureContents and 'fields' in self.secureContents:
            for field in self.secureContents['fields']:
                if field.get('designator', None) == 'username':
                    return field['value']
            for field in self.secureContents['fields']:
                if field.get('name', None) == 'username':
                    return field['value']
            for field in self.secureContents['fields']:
                if field.get('name', None) == 'login':
                    return field['value']
            for field in self.secureContents['fields']:
                if field.get('type', None) == 'T':
                    return field['value']
        return ''

    def findPassword(self):
        if self.secureContents:
            if 'password' in self.secureContents:
                return self.secureContents['password']
            elif 'fields' in self.secureContents:
                for field in self.secureContents['fields']:
                    if field.get('designator', None) == 'password':
                        return field['value']
                for field in self.secureContents['fields']:
                    if field.get('name', None) == 'password':
                        return field['value']
                for field in self.secureContents['fields']:
                    if field.get('type', None) == 'P':
                        return field['value']
                if 'reg_code' in self.secureContents:
                    return self.secureContents['reg_code']
        return ''

    def findUrl(self):
        if self.secureContents and 'URLs' in self.secureContents:
            for field in self.secureContents['URLs']:
                if field.get('url', None) is not None:
                    return field['url']
        return None

    def findNotes(self):
        if self.secureContents:
            if 'notesPlain' in self.secureContents:
                return self.secureContents['notesPlain']
        return None

    def findExpiryTime(self):
        return None

    def findTags(self):
        if self.openContents and 'tags' in self.openContents:
            return self.openContents['tags']
        return None

    def findIcon(self):
        if self.secureContents:
            if 'customIcon' in self.secureContents:
                b64_bytes = self.secureContents['customIcon'].encode('ascii')
                return base64.b64decode(b64_bytes)
        return None
