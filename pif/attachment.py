from pathlib import Path


class PifAttachment:
    def __init__(self, path: Path):
        self.path = path
        self.protected = True

    @property
    def data(self) -> bytes:
        with self.path.open('rb') as f:
            return f.read()

    @property
    def filename(self) -> str:
        return self.path.name
