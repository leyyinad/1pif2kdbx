import json
import logging
import os
from pathlib import Path
from typing import IO, Dict, List

from .pifentry import PifEntry

logger = logging.getLogger(__name__)

DATA_1PIF_FILENAME = 'data.1pif'
PIF_TYPE_FOLDER = 'system.folder.Regular'
PIF_ATT_FOLDER_NAME = 'attachments'


class Parser1PIF:
    def __init__(self, folder: Path) -> None:
        self.folder = folder
        self.pif_entries: List[PifEntry] = []
        self.pif_folders: Dict[str, PifEntry] = dict()

        assert os.path.isdir(folder)

    def parse(self) -> None:
        filename = self.folder / DATA_1PIF_FILENAME
        logger.debug('loading %s', filename)

        with open(filename) as f:
            separator = self.find_separator(f)
            f.seek(0)

            current_entry = ''
            for line in f:
                if separator in line:
                    l, r = line.split(separator, 2)
                    current_entry += l
                    self.add_entry(current_entry.strip())
                    current_entry = r
                else:
                    current_entry += line
            assert current_entry.strip() == ''

    def add_entry(self, s: str) -> None:
        data = json.loads(s)
        uuid = data['uuid']
        entry = PifEntry(data, self.folder / PIF_ATT_FOLDER_NAME / uuid)
        entry.collectAttachments()
        self.pif_entries.append(entry)

        if entry.typeName == PIF_TYPE_FOLDER:
            self.pif_folders[str(entry.uuid)] = entry

    def find_separator(self, f: IO[str]) -> str:
        separator = ''
        for line in f:
            if line.startswith('***'):
                sep_old, separator = separator, line.strip()
                if sep_old != '' and sep_old != separator:
                    logger.warn(
                        'separators differ ("%s" != "%s")',
                        separator,
                        sep_old)
        return separator
